'use strict';

let env = require('dotenv').load();
let parseJSON = require('json-parse-async');
let wsServer = require('./websocket');
let enable_debug = true;


wsServer.on('request', function (request) {
  let connection = request.accept('', request.origin);
  connection.on('message', function (message) {
    parseJSON(message.utf8Data)
      .then(function (content) {
        if (content.cmd == 'ping') {
          let data = { 'pong': true }
          connection.sendUTF(JSON.stringify(data));
        }
        if (content.cmd == 'settings') {
          getSettings();
          let data = { 'success': true }
          connection.sendUTF(JSON.stringify(data));
        }
        if (content.cmd == 'data') {
          console.log(content);
          manageData(content);
        }
      })
  });
});

let vBatThreshold = 5.6;
let cpuTempThreshold = 10;
let hddTempThreshold = 10;
let volumeThreshold = 90;
let gpuTempThreshold = 10;
let freeMemThreshold = 5;
let cpuLoadMonitor = true;

let models = require("./models");

const Op = models.Sequelize.Op;

let Device = models.device,
  Settings = models.settings,
  Alert = models.alert,
  HardwareCpu = models.hardwareCpu,
  HardwareGpu = models.hardwareGpu,
  HardwareHdd = models.hardwareHdd,
  HardwareAdapter = models.hardwareAdapter,
  HardwareAdapterIp = models.hardwareAdapterIp,
  HardwareMb = models.hardwareMb,
  HardwareRam = models.hardwareRam,
  HardwareVolume = models.hardwareVolume,
  HardwarePrinter = models.hardwarePrinter,
  HardwareDisplay = models.hardwareDisplay,
  SoftwareList = models.softwareList,
  LocalUser = models.localUser,
  Log = models.log;

setLog('Запуск сервиса');
getSettings();

function debug(message) {
  var nowDate = new Date().toString();
  if (enable_debug) {
    console.log('[' + nowDate + ']' + message);
  }
}

function manageData(content) {
  Device.findOne({
    where: { name: content.name }
  }).then(result => {
    if (!result) {
      setDevice(content);
    } else {
      updateDevice(content);
      setLastOnline(content.name);
    }
  });
}

function getSettings() {
  Settings.findOne({
    where: { id: 1 }
  }).then(function (result) {
    parseJSON(result.value)
      .then(function (settings) {
        vBatThreshold = settings['vBatThreshold'];
        cpuTempThreshold = settings['cpuTempThreshold'];
        hddTempThreshold = settings['hddTempThreshold'];
        volumeThreshold = settings['volumeThreshold'];
        gpuTempThreshold = settings['gpuTempThreshold'];
        freeMemThreshold = settings['freeMemThreshold'];
        cpuLoadMonitor = settings['cpuLoadMonitor']
      });
  });
  setLog('Загрузка настроек');
}

let pushObject = function (object, value) {
  Object.keys(object).map(function (key, index) {
    object[key]['device_id'] = value;
  });
}

function setDevice(data) {
  setLog('Обнаружен новый компьютер с именем ' + data.name);
  Device.create(data, { include: [{ all: true }] }).then(result => {
    pushObject(data.adapter, result.id);
    setHardwareAdapter(data.adapter);
    analyze(data);
  });
}

function updateDevice(data) {
  Device.update(data, { where: { name: data.name } }).then(result => {
    Device.findOne({
      where: { name: data.name }
    }).then(result => {
      pushObject(data.hdd, result.id);
      pushObject(data.adapter, result.id);
      pushObject(data.cpu, result.id);
      pushObject(data.gpu, result.id);
      pushObject(data.mb, result.id);
      pushObject(data.ram, result.id);
      pushObject(data.volume, result.id);
      pushObject(data.user, result.id);
      pushObject(data.printer, result.id);
      pushObject(data.display, result.id);
      pushObject(data.software, result.id);
      updateHardwareHdd(data.hdd);
      updateHardwareAdapter(data.adapter);
      updateHardwareCpu(data.cpu);
      updateHardwareGpu(data.gpu);
      updateHardwareMb(data.mb);
      updateHardwareRam(data.ram);
      updateHardwareVolume(data.volume);
      updateLocalUser(data.user);
      updateHardwarePrinter(data.printer);
      updateHardwareDisplay(data.display);
      updateSoftware(data.software);
      analyze(data);
    })
  });
}

function analyze(data) {

  if (cpuLoadMonitor) {
    try {
      if (data.telemetry[0].highCpuLoad) {
        let type = 7;
        Device.findOne({
          where: { name: data.name }
        }).then(device => {
          if (device) {
            Alert.findOne({
              where: { device_id: device.id, type: type }
            }).then(alert => {
              if (!alert) {
                Alert.create({ device_id: device.id, description: 'Высокая загрузка процессора в течение продолжительного времени', type: type });
              }
            })
          }
        })
      }
    } catch (e) {
      //console.log(e);
      //setLog('Ошибка анализа загрузки процессора ' + data.name);
    }
  }

  try {
    if (data.telemetry[0].vBat !== 0 && data.telemetry[0].vBat< vBatThreshold) {
      let type = 1;
      Device.findOne({
        where: { name: data.name }
      }).then(device => {
        if (device) {
          Alert.findOne({
            where: { device_id: device.id, type: type }
          }).then(alert => {
            if (!alert) {
              Alert.create({ device_id: device.id, description: 'Низкое напряжение батарейки БИОС < ' + vBatThreshold + 'V', type: type });
            }
          })
        }
      })
    }
  } catch (e) {
    //console.log(e);
    //setLog('Ошибка анализа заряда батарейки БИОС ' + data.name);
  }

  try {
    if (data.telemetry[0].cpuTemp > cpuTempThreshold) {
      let type = 2;
      Device.findOne({
        where: { name: data.name }
      }).then(device => {
        if (device) {
          Alert.findOne({
            where: { device_id: device.id, type: type }
          }).then(alert => {
            if (!alert) {
              Alert.create({ device_id: device.id, description: 'Температура процессора более ' + cpuTempThreshold + 'C', type: type });
            }
          })
        }
      })
    }
  } catch (e) {
    //console.log(e);
    //setLog('Ошибка анализа температуры процессора ' + data.name);
  }

  try {
    if (((data.telemetry[0].freeMem / (data.telemetry[0].totalMem / 1024)) * 100) < freeMemThreshold) {
      let type = 6;
      Device.findOne({
        where: { name: data.name }
      }).then(device => {
        if (device) {
          Alert.findOne({
            where: { device_id: device.id, type: type }
          }).then(alert => {
            if (!alert) {
              Alert.create({ device_id: device.id, description: 'Недостаточно оперативной памяти < ' + freeMemThreshold + '%', type: type });
            }
          })
        }
      })
    }
  } catch (e) {
    //console.log(e);
    //setLog('Ошибка анализа свободной памяти ' + data.name);
  }

  try {
    data.hdd.forEach(function (item) {
      if (item.hddTemp > hddTempThreshold) {
        let type = 3;
        Device.findOne({
          where: { name: data.name }
        }).then(device => {
          if (device) {
            Alert.findOne({
              where: { device_id: device.id, type: type }
            }).then(alert => {
              if (!alert) {
                Alert.create({ device_id: device.id, description: 'Температура жесткого диска ' + item.name + ' более ' + hddTempThreshold + 'C', type: type });
              }
            })
          }
        })
      }
    });
  } catch (e) {
    //console.log(e);
    //setLog('Ошибка анализа температуры жестких дисков ' + data.name);
  }

  try {
    data.volume.forEach(function (item) {
      if (Math.round((parseInt(item.free) / parseInt(item.total)) * 100) < volumeThreshold) {
        let type = 4;
        Device.findOne({
          where: { name: data.name }
        }).then(device => {
          if (device) {
            Alert.findOne({
              where: { device_id: device.id, type: type, description: { [Op.like]: '%' + item.name + '%' } }
            }).then(alert => {
              if (!alert) {
                Alert.create({ device_id: device.id, description: 'На томе ' + item.name + ' осталось менее ' + volumeThreshold + '% свободного места', type: type });
              }
            })
          }
        })
      }
    });
  } catch (e) {
    //setLog('Ошибка анализа места на томах ' + data.name);
    //console.log(e);
  }

  try {
    data.gpu.forEach(function (item) {
      if (item.gpuTemp > gpuTempThreshold) {
        let type = 5;
        Device.findOne({
          where: { name: data.name }
        }).then(device => {
          if (device) {
            Alert.findOne({
              where: { device_id: device.id, type: type }
            }).then(alert => {
              if (!alert) {
                Alert.create({ device_id: device.id, description: 'Температура видеокарты ' + item.name + ' более ' + gpuTempThreshold + 'C', type: type });
              }
            })
          }
        })
      }
    });
  } catch (e) {
    //console.log(e);
    //setLog('Ошибка температуры видеокарты ' + data.name);
  }

}

function setLog(value) {
  Log.create({ value: value }).then();
}

function setLastOnline(name) {
  Device.update({ last_online: models.sequelize.fn('NOW') }, { where: { name: name } }).then();
}
function setHardwareAdapter(data) {
  data.forEach(function (item) {
    HardwareAdapter.create({ name: item.name, device_id: data[0].device_id, mac: item.mac }).then(result => {
      item.ip.forEach(function (address) {
        HardwareAdapterIp.create({ address: address, adapter_id: result.id });
      });
    });
  })
}

function updateHardwareHdd(data) {
  HardwareHdd.findAll({
    where: { device_id: data[0].device_id }
  }).then(function (hddItems) {
    data.forEach(function (item) {
      let found = hddItems.find(x => x.name === item.name);
      if (!found) {
        HardwareHdd.create({ name: item.name, device_id: data[0].device_id });
      }
    })
    hddItems.forEach(function (item) {
      let found = data.find(x => x.name === item.name);
      if (!found) {
        HardwareHdd.destroy({ where: { name: item.name, device_id: data[0].device_id } });
      }
    })
  })
}

function updateHardwareAdapter(data) {
  HardwareAdapter.findAll({
    where: { device_id: data[0].device_id }, include: [{ all: true }]
  }).then(function (adapterItems) {
    data.forEach(function (item) {
      let found = adapterItems.find(x => x.name === item.name);
      if (!found) {
        HardwareAdapter.create({ name: item.name, device_id: data[0].device_id, mac: item.mac }).then(result => {
          item.ip.forEach(function (address) {
            HardwareAdapterIp.create({ address: address, adapter_id: result.id });
          });
        });
      }
    })
    adapterItems.forEach(function (item) {
      let found = data.find(x => x.name === item.name);
      if (!found) {
        HardwareAdapterIp.destroy({ where: { adapter_id: item.id } });
        HardwareAdapter.destroy({ where: { name: item.name, device_id: data[0].device_id } });
      }
    })
  })
}

function updateHardwareCpu(data) {
  data.forEach(function (item) {
    HardwareCpu.update({ name: item.name }, { where: { device_id: data[0].device_id } });
  });
}

function updateHardwareGpu(data) {
  HardwareGpu.findAll({
    where: { device_id: data[0].device_id }
  }).then(function (gpuItems) {
    data.forEach(function (item) {
      let found = gpuItems.find(x => x.name === item.name);
      if (!found) {
        HardwareGpu.create({ name: item.name, device_id: data[0].device_id });
      }
    })
    gpuItems.forEach(function (item) {
      let found = data.find(x => x.name === item.name);
      if (!found) {
        HardwareGpu.destroy({ where: { name: item.name, device_id: data[0].device_id } });
      }
    })
  })
}

function updateHardwareVolume(data) {
  HardwareVolume.findAll({
    where: { device_id: data[0].device_id }
  }).then(function (volumeItems) {
    data.forEach(function (item) {
      HardwareVolume.update(item, { where: { name: item.name, device_id: data[0].device_id } });
      let found = volumeItems.find(x => x.name === item.name);
      if (!found) {
        HardwareVolume.create(item);
      }
    })
    volumeItems.forEach(function (item) {
      let found = data.find(x => x.name === item.name);
      if (!found) {
        HardwareVolume.destroy({ where: { name: item.name, device_id: data[0].device_id } });
      }
    })
  })
}

function updateHardwareMb(data) {
  data.forEach(function (item) {
    HardwareMb.update({ name: item.name }, { where: { device_id: data[0].device_id } });
  });
}

function updateHardwareRam(data) {
  HardwareRam.findAll({
    where: { device_id: data[0].device_id }
  }).then(function (ramItems) {
    data.forEach(function (item) {
      let found = ramItems.find(x => x.serial_number === item.serial_number);
      if (!found) {
        HardwareRam.create(item);
      }
    })
    ramItems.forEach(function (item) {
      let found = data.find(x => x.serial_number === item.serial_number);
      if (!found) {
        HardwareRam.destroy({ where: { serial_number: item.serial_number, device_id: data[0].device_id } });
      }
    })
  })
}

function updateLocalUser(data) {
  LocalUser.findAll({
    where: { device_id: data[0].device_id }
  }).then(function (userItems) {
    data.forEach(function (item) {
      LocalUser.update(item, { where: { name: item.name, device_id: data[0].device_id } });
      let found = userItems.find(x => x.name === item.name);
      if (!found) {
        LocalUser.create(item);
      }
    })
    userItems.forEach(function (item) {
      let found = data.find(x => x.name === item.name);
      if (!found) {
        LocalUser.destroy({ where: { name: item.name, device_id: data[0].device_id } });
      }
    })
  })
}

function updateHardwarePrinter(data) {
  HardwarePrinter.findAll({
    where: { device_id: data[0].device_id }
  }).then(function (printerItems) {
    data.forEach(function (item) {
      HardwarePrinter.update(item, { where: { name: item.name, device_id: data[0].device_id } });
      let found = printerItems.find(x => x.name === item.name);
      if (!found) {
        HardwarePrinter.create(item);
      }
    })
    printerItems.forEach(function (item) {
      let found = data.find(x => x.name === item.name);
      if (!found) {
        HardwarePrinter.destroy({ where: { name: item.name, device_id: data[0].device_id } });
      }
    })
  })
}

function updateHardwareDisplay(data) {
  HardwareDisplay.findAll({
    where: { device_id: data[0].device_id }
  }).then(function (displayItems) {
    data.forEach(function (item) {
      let found = displayItems.find(x => x.name === item.name);
      if (!found) {
        HardwareDisplay.create({ name: item.name, device_id: data[0].device_id });
      }
    })
    displayItems.forEach(function (item) {
      let found = data.find(x => x.name === item.name);
      if (!found) {
        HardwareDisplay.destroy({ where: { name: item.name, device_id: data[0].device_id } });
      }
    })
  })
}

function updateSoftware(data) {
  SoftwareList.findAll({
    where: { device_id: data[0].device_id }
  }).then(function (softwareItems) {
    data.forEach(function (item) {
      let found = softwareItems.find(x => x.name === item.name);
      if (!found) {
        SoftwareList.create({ name: item.name, device_id: data[0].device_id, publisher: item.publisher, version: item.version });
      }
    })
    softwareItems.forEach(function (item) {
      let found = data.find(x => x.name === item.name);
      if (!found) {
        SoftwareList.destroy({ where: { name: item.name, device_id: data[0].device_id } });
      }
    })
  })
}
