module.exports = function (sequelize, Sequelize) {

  var SoftwareList = sequelize.define('softwareList', {

    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    name: {
      type: Sequelize.STRING,
      notEmpty: true
    },

    publisher: {
      type: Sequelize.STRING,
      notEmpty: true
    },

    version: {
      type: Sequelize.STRING,
      notEmpty: true
    },

    device_id: {
      type: Sequelize.INTEGER,
      notEmpty: true
    },

  },

    {
      tableName: 'software_list',
      createdAt: false,
      updatedAt: false,
      underscored: true
    }

  );

  SoftwareList.associate = function (models) {
    SoftwareList.belongsTo(models.device, { foreignKey: 'device_id', targetKey: 'id' });
  } 

  return SoftwareList;

}