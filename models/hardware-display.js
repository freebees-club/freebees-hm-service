module.exports = function (sequelize, Sequelize) {

  var HardwareDisplay = sequelize.define('hardwareDisplay', {

    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    device_id: {
      type: Sequelize.INTEGER,
      notEmpty: true
    },

    name: {
      type: Sequelize.STRING,
      notEmpty: true
    },

    resolution: {
      type: Sequelize.STRING
    },    

  },

    {
      tableName: 'hardware_display',
      createdAt: false,
      updatedAt: false,
      underscored: true
    }

  );

  HardwareDisplay.associate = function (models) {
    HardwareDisplay.belongsTo(models.device, { foreignKey: 'device_id', targetKey: 'id' });
  }

  return HardwareDisplay;

}