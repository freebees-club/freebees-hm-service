module.exports = function (sequelize, Sequelize) {

  var Log = sequelize.define('log', {

    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    value: {
      type: Sequelize.STRING,
      notEmpty: true
    },

    created_at: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    }

  },

    {
      tableName: 'log',
      updatedAt: false,
      underscored: true
    }

  );

  return Log;

}