module.exports = function (sequelize, Sequelize) {

  var HardwareHdd = sequelize.define('hardwareHdd', {

    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    device_id: {
      type: Sequelize.INTEGER,
      notEmpty: true
    },

    name: {
      type: Sequelize.STRING,
      notEmpty: true
    },

  },

    {
      tableName: 'hardware_hdd',
      createdAt: false,
      updatedAt: false,
      underscored: true
    }

  );

  HardwareHdd.associate = function (models) {
    HardwareHdd.belongsTo(models.device, { foreignKey: 'device_id', targetKey: 'id' });
  }  

  return HardwareHdd;

}