module.exports = function (sequelize, Sequelize) {

  var HardwareGpu = sequelize.define('hardwareGpu', {

    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    device_id: {
      type: Sequelize.INTEGER,
      notEmpty: true
    },

    name: {
      type: Sequelize.STRING,
      notEmpty: true
    },

  },

    {
      tableName: 'hardware_gpu',
      createdAt: false,
      updatedAt: false,
      underscored: true
    }

  );

  HardwareGpu.associate = function (models) {
    HardwareGpu.belongsTo(models.device, { foreignKey: 'device_id', targetKey: 'id' });
  }

  return HardwareGpu;

}