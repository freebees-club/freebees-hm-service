module.exports = function (sequelize, Sequelize) {

  var HardwareAdapter = sequelize.define('hardwareAdapter', {

    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    device_id: {
      type: Sequelize.INTEGER,
      notEmpty: true
    },

    name: {
      type: Sequelize.STRING,
      notEmpty: true
    },

    mac: {
      type: Sequelize.STRING,
      notEmpty: true
    },

  },

    {
      tableName: 'hardware_adapter',
      createdAt: false,
      updatedAt: false,
      underscored: true,
    }

  );

  HardwareAdapter.associate = function (models) {
    HardwareAdapter.hasMany(models.hardwareAdapterIp, { as: 'ip', foreignKey: 'adapter_id' });
  }

  return HardwareAdapter;

}