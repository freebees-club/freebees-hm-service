module.exports = function (sequelize, Sequelize) {

  var HardwareVolume = sequelize.define('hardwareVolume', {

    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    device_id: {
      type: Sequelize.INTEGER,
      notEmpty: true
    },

    name: {
      type: Sequelize.STRING,
      notEmpty: true
    },
    
    label: {
      type: Sequelize.STRING,
      notEmpty: true
    },

    format: {
      type: Sequelize.STRING,
      notEmpty: true
    },

    available: {
      type: Sequelize.INTEGER,
      notEmpty: true
    },

    free: {
      type: Sequelize.INTEGER,
      notEmpty: true
    },

    total: {
      type: Sequelize.INTEGER,
      notEmpty: true
    },

    type: {
      type: Sequelize.STRING,
      notEmpty: true
    },

  },

    {
      tableName: 'hardware_volume',
      createdAt: false,
      updatedAt: false,
      underscored: true
    }

  );

  HardwareVolume.associate = function (models) {
    HardwareVolume.belongsTo(models.device, { foreignKey: 'device_id', targetKey: 'id' });
  } 

  return HardwareVolume;

}