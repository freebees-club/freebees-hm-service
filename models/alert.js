module.exports = function (sequelize, Sequelize) {

  var Alert = sequelize.define('alert', {

    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    description: {
      type: Sequelize.STRING,
      notEmpty: true
    },

    type: {
      type: Sequelize.INTEGER,
      notEmpty: true
    },

    device_id: {
      type: Sequelize.INTEGER,
      notEmpty: true
    },       

    created_at: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    }

  },

    {
      tableName: 'alert',
      updatedAt: false,
      underscored: true
    }

  );

  return Alert;

}