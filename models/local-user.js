module.exports = function (sequelize, Sequelize) {

  var LocalUser = sequelize.define('localUser', {

    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    device_id: {
      type: Sequelize.INTEGER,
      notEmpty: true
    },

    name: {
      type: Sequelize.STRING,
      notEmpty: true
    },
    
    full_name: {
      type: Sequelize.STRING
    },

    description: {
      type: Sequelize.STRING
    },

    is_local: {
      type: Sequelize.BOOLEAN
    },

    domain: {
      type: Sequelize.STRING
    },

    is_disabled: {
      type: Sequelize.BOOLEAN
    },   

  },

    {
      tableName: 'local_user',
      createdAt: false,
      updatedAt: false,
      underscored: true
    }

  );

  LocalUser.associate = function (models) {
    LocalUser.belongsTo(models.device, { foreignKey: 'device_id', targetKey: 'id' });
  } 

  return LocalUser;

}