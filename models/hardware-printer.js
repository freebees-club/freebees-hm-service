module.exports = function (sequelize, Sequelize) {

  var HardwarePrinter = sequelize.define('hardwarePrinter', {

    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    device_id: {
      type: Sequelize.INTEGER,
      notEmpty: true
    },

    name: {
      type: Sequelize.STRING,
      notEmpty: true
    },
    
    description: {
      type: Sequelize.STRING
    },

    detected_error_state: {
      type: Sequelize.INTEGER
    },

    driver_name: {
      type: Sequelize.STRING
    },

    port_name: {
      type: Sequelize.STRING
    },    

    share_name: {
      type: Sequelize.STRING
    },

    is_local: {
      type: Sequelize.BOOLEAN
    },

    is_network: {
      type: Sequelize.BOOLEAN
    },

    is_shared: {
      type: Sequelize.BOOLEAN
    },

    is_default: {
      type: Sequelize.BOOLEAN
    },    

  },

    {
      tableName: 'hardware_printer',
      createdAt: false,
      updatedAt: false,
      underscored: true
    }

  );

  HardwarePrinter.associate = function (models) {
    HardwarePrinter.belongsTo(models.device, { foreignKey: 'device_id', targetKey: 'id' });
  }   

  return HardwarePrinter;

}