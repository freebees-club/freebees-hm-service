module.exports = function (sequelize, Sequelize) {

  var HardwareRam = sequelize.define('hardwareRam', {

    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    device_id: {
      type: Sequelize.INTEGER,
      notEmpty: true
    },

    manufacturer: {
      type: Sequelize.STRING
    },

    capacity: {
      type: Sequelize.INTEGER
    },
    
    speed: {
      type: Sequelize.INTEGER
    },  
    
    memory_type: {
      type: Sequelize.STRING
    },
    
    memory_type: {
      type: Sequelize.STRING
    },

    part_number: {
      type: Sequelize.STRING
    },
    
    serial_number: {
      type: Sequelize.STRING
    },       

  },

    {
      tableName: 'hardware_ram',
      createdAt: false,
      updatedAt: false,
      underscored: true
    }

  );

  HardwareRam.associate = function (models) {
    HardwareRam.belongsTo(models.device, { foreignKey: 'device_id', targetKey: 'id' });
  }     

  return HardwareRam;

}