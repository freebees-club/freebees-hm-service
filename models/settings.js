module.exports = function (sequelize, Sequelize) {

  var Settings = sequelize.define('settings', {

    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    value: {
      type: Sequelize.STRING,
      notEmpty: true
    },

    created_at: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    }

  },

    {
      tableName: 'settings',
      updatedAt: false,
      underscored: true
    }

  );

  return Settings;

}