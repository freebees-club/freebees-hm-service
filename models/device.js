module.exports = function (sequelize, Sequelize) {

  var Device = sequelize.define('device', {

    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    name: {
      type: Sequelize.STRING,
      notEmpty: true
    },

    description: {
      type: Sequelize.STRING,
      notEmpty: false
    },

     last_online: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    },

    os_version: {
      type: Sequelize.STRING,
    },

    service_version: {
      type: Sequelize.STRING,
    },

    group_id: {
      type: Sequelize.INTEGER,
      defaultValue: 1,
      allowNull: false
    },

    logged_on_user: {
      type: Sequelize.STRING
    },

    uptime: {
      type: Sequelize.STRING
    },

    ram_total: {
      type: Sequelize.INTEGER
    },

    created_at: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    }

  },

    {
      tableName: 'device',
      updatedAt: false,
      underscored: true
    }

  );

  Device.associate = function (models) {
    Device.hasMany(models.hardwareCpu, { foreignKey: 'device_id', targetKey: 'id', as: 'cpu' });
    Device.hasMany(models.hardwareGpu, { foreignKey: 'device_id', targetKey: 'id', as: 'gpu' });
    Device.hasMany(models.hardwareDisplay, { foreignKey: 'device_id', targetKey: 'id', as: 'display' });
    Device.hasMany(models.hardwareHdd, { foreignKey: 'device_id', targetKey: 'id', as: 'hdd' });
    Device.hasMany(models.hardwareMb, { foreignKey: 'device_id', targetKey: 'id', as: 'mb' });
    Device.hasMany(models.hardwarePrinter, { foreignKey: 'device_id', targetKey: 'id', as: 'printer' });
    Device.hasMany(models.hardwareRam, { foreignKey: 'device_id', targetKey: 'id', as: 'ram' });
    Device.hasMany(models.hardwareVolume, { foreignKey: 'device_id', targetKey: 'id', as: 'volume' });
    Device.hasMany(models.localUser, { foreignKey: 'device_id', targetKey: 'id', as: 'user' });
    Device.hasMany(models.softwareList, { foreignKey: 'device_id', targetKey: 'id', as: 'software' });
    //Device.hasMany(models.hardwareAdapter, { foreignKey: 'device_id', targetKey: 'id', as: 'adapter' });
  }

  return Device;

}