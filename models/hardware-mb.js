module.exports = function (sequelize, Sequelize) {

  var HardwareMb = sequelize.define('hardwareMb', {

    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    device_id: {
      type: Sequelize.INTEGER,
      notEmpty: true
    },

    name: {
      type: Sequelize.STRING,
      notEmpty: true
    },

  },

    {
      tableName: 'hardware_mb',
      createdAt: false,
      updatedAt: false,
      underscored: true
    }

  );

  HardwareMb.associate = function (models) {
    HardwareMb.belongsTo(models.device, { foreignKey: 'device_id', targetKey: 'id' });
  }

  return HardwareMb;

}