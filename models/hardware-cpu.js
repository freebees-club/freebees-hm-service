module.exports = function (sequelize, Sequelize) {

  var HardwareCpu = sequelize.define('hardwareCpu', {

    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    device_id: {
      type: Sequelize.INTEGER,
      notEmpty: true
    },

    name: {
      type: Sequelize.STRING,
      notEmpty: true
    },

  },

    {
      tableName: 'hardware_cpu',
      createdAt: false,
      updatedAt: false,
      underscored: true,
    }

  );

  HardwareCpu.associate = function (models) {
    HardwareCpu.belongsTo(models.device, { foreignKey: 'device_id', targetKey: 'id' });
  }

  return HardwareCpu;

}