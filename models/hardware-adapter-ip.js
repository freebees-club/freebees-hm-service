module.exports = function (sequelize, Sequelize) {

  var HardwareAdapterIp = sequelize.define('hardwareAdapterIp', {

    id: {
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },

    adapter_id: {
      type: Sequelize.INTEGER,
      notEmpty: true
    },

    address: {
      type: Sequelize.STRING,
      notEmpty: true
    },

  },

    {
      tableName: 'hardware_adapter_ip',
      createdAt: false,
      updatedAt: false,
      underscored: true,
    }

  );

  HardwareAdapterIp.associate = function (models) {
    HardwareAdapterIp.belongsTo(models.hardwareAdapter, { foreignKey: 'adapter_id' });
  }

  return HardwareAdapterIp;

}