'use strict'

let WebSocketServer = require('websocket').server;
let http = require('http');
let server = http.createServer(function (request, response) {
  response.writeHead(404);
  response.end();
});
let wsServer = new WebSocketServer({
  httpServer: server
});
server.listen(3000);

module.exports = wsServer;